package gameMinikaeva.characters;
import java.util.Random;
import gameMinikaeva.todo.*;

public class TeamLead extends ITeam{
  //"TeamLead. I'm gonna fix it quickly!"

  public TeamLead(String message){
    super(message);
    itBonus = 2;
  }

  public void setMessage(String message){
    this.message = message;
  }

  public double doCountGamer(Gamer gamer){
    gamer.score += 30000;
    gamer.score = this.bonusGamer(gamer);
    return gamer.score;
  }

  public double bonusGamer(Gamer gamer){
    Random rnd = new Random();
    int bonusChance = rnd.nextInt(2)+1;
    if (bonusChance == 1){
      gamer.score *= this.itBonus;
    }
    else{
      gamer.score /= this.itBonus;
    }
    System.out.println("Ваш счет: " + gamer.score);
    return gamer.score;
  }
}
