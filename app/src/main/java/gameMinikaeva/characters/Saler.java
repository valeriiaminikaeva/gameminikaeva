package gameMinikaeva.characters;
import gameMinikaeva.todo.*;
import java.util.Random;

public class Saler extends Business {
  //"Saler. I'm gonna fix it quickly!"

  public Saler(String message){
    super(message);
    businessBonus = 1.5;
  }

  public void setMessage(String message){
    this.message = message;
  }

  public double doCountGamer(Gamer gamer){
    gamer.score += 35000;
    gamer.score = this.bonusGamer(gamer);
    return gamer.score;
  }

  public double bonusGamer(Gamer gamer){
    Random rnd = new Random();
    int bonusChance = rnd.nextInt(2)+1;
    if (bonusChance == 1){
      gamer.score *= this.businessBonus;
    }
    else{
      gamer.score /= this.businessBonus;
    }
    System.out.println("Ваш счет: " + gamer.score);
    return gamer.score;
  }
}
