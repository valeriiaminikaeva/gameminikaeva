package gameMinikaeva.characters;
import gameMinikaeva.todo.*;
import java.util.Random;

public class Hacker extends ITeam {
  //"Hacker. I'm gonna make our rival suffer!"

  public Hacker(String message){
    super(message);
    itBonus = 1.5;
  }

  public void setMessage(String message){
    this.message = message;
  }

  public double doCountGamer(Gamer gamer){
    gamer.score += 40000;
    gamer.score = this.bonusGamer(gamer);
    return gamer.score;
  }

  public double bonusGamer(Gamer gamer){
    Random rnd = new Random();
    int bonusChance = rnd.nextInt(2)+1;
    if (bonusChance == 1){
      gamer.score *= this.itBonus;
    }
    else{
      gamer.score /= this.itBonus;
    }
    System.out.println("Ваш счет: " + gamer.score);
    return gamer.score;
  }
}
