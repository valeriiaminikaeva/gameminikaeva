package gameMinikaeva.characters;
import java.util.Random;
import gameMinikaeva.todo.*;

public abstract class MyCharacter{
  protected String message;

  // private static int power;

  public MyCharacter(String message){
    this.message = message;
  }

  public String getMessage(){
    return this.message;
  }

  public abstract double doCountGamer(Gamer gamer);
  public abstract double bonusGamer(Gamer gamer);
}
