package gameMinikaeva.todo;

import java.util.Scanner;
import gameMinikaeva.actions.*;
import gameMinikaeva.characters.*;
import gameMinikaeva.todo.*;

public class App {

  public static MenuItem buildMenu() throws Exception{
    MenuItem begin = new MenuItem("My game");
    Action one = new ActionTwo("Играть!");
    begin.setAction(one);

    return begin;
  }

  public static void main(String[] args) throws Exception{
    MenuItem begin = buildMenu();
    System.out.println("Ситуация1");
    Scanner in = new Scanner(System.in);
    int userResponse = 0;

    while(true){
      begin.printStr();
      userResponse = in.nextInt();

      System.out.println();
      if (userResponse == 0){
        if (begin.parent == null){
          break;
        }
      }
      userResponse--;
      if (userResponse > begin.actions.size() && userResponse <= begin.actions.size() + begin.children.size()){
        begin = begin.children.get(userResponse - begin.actions.size());
        System.out.println("ok");
      }
      if((userResponse >= 0) && (userResponse < begin.actions.size())){
        begin.actions.get(userResponse).act();
        System.out.println("ok1");
      }
    }
  }

}
