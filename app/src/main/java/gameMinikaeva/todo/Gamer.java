package gameMinikaeva.todo;
import gameMinikaeva.actions.*;
import gameMinikaeva.characters.MyCharacter;

public class Gamer{
  public double score = 100000;
  static MyCharacter[] charactersLevel1;
  static MyCharacter[] charactersLevel2;
  static MyCharacter[] charactersLevel3;

  public Gamer(MyCharacter[] charactersLevel1){
    this.charactersLevel1 = charactersLevel1;
  }

  public double getScore(){
    return this.score;
  }

  public void getCharactersLevel1(){
    for(int i = 0; i < charactersLevel1.length; i++){
      System.out.println(i + ". " + charactersLevel1[i].getMessage());
    }
  }

  // abstract void getCharactersLevel2();
  //
  // abstract void getCharactersLevel3();
}
