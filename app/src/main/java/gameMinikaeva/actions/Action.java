package gameMinikaeva.actions;

public abstract class Action{
  public String str;

  public Action(String str){
    this.str = str;
  }

  public String getStr(){
    return this.str;
  }

  public abstract void act() throws Exception;



}
