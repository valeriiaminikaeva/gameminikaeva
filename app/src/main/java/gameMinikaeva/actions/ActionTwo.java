package gameMinikaeva.actions;

import java.util.Scanner;
import gameMinikaeva.todo.*;
import gameMinikaeva.characters.*;

public class ActionTwo extends Action{

  public ActionTwo(String str){
    super(str);
  }
  //"Hacker. I'm gonna make our rival suffer!"
  public void act() throws Exception{
    //for gamer 1
    MyCharacter[] g1L1 = new MyCharacter[2];
    Gamer gamer1 = new Gamer(g1L1);
    g1L1[0] = new Hacker("Hacker. I'm gonna make our rival suffer!");
    g1L1[1] = new Ceo("Ceo. I'm gonna fix it quickly!");
    System.out.println("Ход игрока1");
    gamer1.getCharactersLevel1();
    int chooseGamer1 = checkingInt("Введите номер персонажа: ");
    MyCharacter buffer1 = g1L1[chooseGamer1];
    if(buffer1 instanceof ITeam){
      buffer1.doCountGamer(gamer1);
    }

    //for gamer 2
    MyCharacter[] g2L1 = new MyCharacter[2];
    Gamer gamer2 = new Gamer(g2L1);
    g2L1[0] = new TeamLead("TeamLead. I'm gonna fix it quickly!");
    g2L1[1] = new Saler("Saler. I'm gonna fix it quickly!");
    System.out.println("Ход игрока2");
    gamer2.getCharactersLevel1();
    int chooseGamer2 = checkingInt("Введите номер персонажа: ");
    MyCharacter buffer2 = g2L1[chooseGamer2];
    if(buffer2 instanceof ITeam){
      buffer2.doCountGamer(gamer2);
    }
  }

  public Integer checkingInt(String str) throws Exception{
    Scanner in = new Scanner(System.in);
    boolean flag = true;
    while (flag){
      System.out.println(str);
      String s = in.nextLine();
      int a;
      try{
        a = Integer.parseInt(s);
        System.out.println();
        return a;
      }catch(Exception e){}
    }
    return null;
  }
}
